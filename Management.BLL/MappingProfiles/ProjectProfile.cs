﻿using Management.DAL.Models;
using AutoMapper;
using Management.Common.DTO;

namespace Management.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
        }
    }
}
