﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        { }
        public TaskDTO CreateTask(TaskDTO newTask)
        {
            var taskEntity = _mapper.Map<Task>(newTask);
            _context.TasksRepo.Create(taskEntity);
            var createdTask = _context.TasksRepo.Get(taskEntity.Id);
            _context.Save();
            return _mapper.Map<TaskDTO>(createdTask);
        }
        public ICollection<TaskDTO> GetTasks()
        {
            var tasks = _context.TasksRepo.GetAll().ToList();
            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }
        public TaskDTO GetTaskById(int id)
        {
            Task task = _context.TasksRepo.Get(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public TaskDTO UpdateTask(TaskDTO task)
        {
            var result = _mapper.Map<Task>(task);
            _context.TasksRepo.Update(result);
            _context.Save();
            return _mapper.Map<TaskDTO>(_context.TasksRepo.Get(result.Id));
        }
        public void DeleteTask(int id)
        {
            var taska = _context.TasksRepo.Get(id);
            if (taska == null) throw new ArgumentException("Invalid id");
            _context.TasksRepo.Delete(taska);
            _context.Save();

        }

        public ICollection<TaskDTO> GetAllTasksWithNameLessThan45Chars(int userId)
        {
            return _mapper.Map<List<TaskDTO>>(_context.TasksRepo.GetAll()).Where(
                    x => x.PerformerId == userId && x.Name.Length < 45).ToList();
        }
        public ICollection<TaskDTO> GetNotFinishedTasksForUser(int userId)
        {
           return _mapper.Map<List<TaskDTO>>(_context.TasksRepo.GetAll()).Where(x => x.State != TaskStateDTO.Finished && x.PerformerId == userId).ToList();
        }

    }
}
