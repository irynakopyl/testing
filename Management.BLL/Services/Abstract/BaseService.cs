﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;

namespace Management.BLL.Services.Abstract
{
    public class BaseService
    {
        private protected readonly IUnitOfWork _context;
        private protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
