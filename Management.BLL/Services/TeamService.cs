﻿using Management.BLL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }
        public TeamDTO CreateTeam(TeamDTO newTeam)
        {
            var teamEntity = _mapper.Map<Team>(newTeam);
            _context.TeamsRepo.Create(teamEntity);
            _context.Save();

            var createdTeam = _context.TeamsRepo.Get(teamEntity.Id);
            return _mapper.Map<TeamDTO>(createdTeam);
        }
        public ICollection<TeamDTO> GetTeams()
        {
            var teams = _context.TeamsRepo.GetAll().ToList();
            return _mapper.Map<ICollection<TeamDTO>>(teams);
        }
        public TeamDTO GetTeamById(int id)
        {
            Team team = _context.TeamsRepo.Get(id);
            return _mapper.Map<TeamDTO>(team);
        }
        public TeamDTO UpdateTeam(TeamDTO team)
        {
            var result = _mapper.Map<Team>(team);
            _context.TeamsRepo.Update(result);
            _context.Save();
            return _mapper.Map<TeamDTO>(_context.TeamsRepo.Get(result.Id));
        }
        public void DeleteTeam(int id)
        {
            var team = _context.TeamsRepo.Get(id);
            if (team == null) throw new ArgumentException("Invalid id");
            _context.TeamsRepo.Delete(team);
            _context.Save();
        }
        public List<TeamListDTO> GetTeamsWithUsersOlderThan10()
        {
            var temas = _mapper.Map<IEnumerable<TeamDTO>>(_context.TeamsRepo.GetAll());
            var uzzers = _mapper.Map<List<UserDTO>>(_context.UsersRepo.GetAll());
            return temas
                .GroupJoin(
                uzzers,
                 team => team.Id,
                 user => user.TeamId,
                (t, u) =>(
                new TeamListDTO
                {
                    Id = t.Id,
                    TeamName = t.Name,
                    Members = u.Where(x=> (2020 - x.Birthday.Year) > 10).OrderByDescending(u => u.RegisteredAt).ToList()
                 })).Where(team=>team.Members.Any()).ToList();
        }
}
}
