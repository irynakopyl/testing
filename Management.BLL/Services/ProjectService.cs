﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }
        public ProjectDTO CreateProject(ProjectDTO newTeam)
        {
            var projectEntity = _mapper.Map<Project>(newTeam);
            _context.ProjectsRepo.Create(projectEntity);
            var createdUsr = _context.ProjectsRepo.Get(projectEntity.Id);
            _context.Save();
            return _mapper.Map<ProjectDTO>(createdUsr);
        }
        public ICollection<ProjectDTO> GetProjects()
        {
            var projects = _context.ProjectsRepo.GetAll().ToList();
            return _mapper.Map<ICollection<ProjectDTO>>(projects);
        }
        public ProjectDTO GetProjectById(int id)
        {
            Project proj = _context.ProjectsRepo.Get(id);
            return _mapper.Map<ProjectDTO>(proj);
        }
        public ProjectDTO UpdateProject(ProjectDTO task)
        {
            var result = _mapper.Map<Project>(task);
            _context.ProjectsRepo.Update(result);
            _context.Save();
            return _mapper.Map<ProjectDTO>(_context.ProjectsRepo.Get(result.Id));
        }
        public void DeleteProject(int id)
        {
            var proj = _context.ProjectsRepo.Get(id);
            if (proj == null) throw new ArgumentException("Invalid id");
            _context.ProjectsRepo.Delete(proj);
            _context.Save();
        }
        public List<ProjectInfoDTO> GetAllProjectsAndItsTasksInfo()
        {
            var projects = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll());
            var users = _mapper.Map<ICollection<UserDTO>>(_context.UsersRepo.GetAll());
            var tasks = _mapper.Map<ICollection<TaskDTO>>(_context.TasksRepo.GetAll());
            return projects.GroupJoin(users,
                p => p.TeamId,
                u => u.TeamId,
                (pr, us) => new ProjectInfoDTO
                {
                    Proj = pr,
                    LongestTask = tasks.Where(t=>t.ProjectId==pr.Id).OrderByDescending(p => p.Description).FirstOrDefault(),
                    ShortestTask = tasks.Where(t => t.ProjectId == pr.Id).OrderBy(p => p.Name).FirstOrDefault(),
                    UserAmount = us.Count()
                }
                ).ToList();




            /*return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Select(t => t.Performer)).Distinct().GroupBy(x => x.TeamId)
               .Select(x => new TeamListDTO
               {
                   Id = x.Key,
                   Members = x.ToList()
               }
               ).Join(
               _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()),
                team => team.Id,
                user => user.TeamId,
               (t, u) =>
               new ProjectInfoDTO
               {
                   Proj = u,
                   LongestTask = u.Tasks.OrderByDescending(p => p.Description).First(),
                   ShortestTask = u.Tasks.OrderBy(p => p.Name).First(),
                   UserAmount = t.Members.Count()
               }).ToList();*/
        }
    }
}
