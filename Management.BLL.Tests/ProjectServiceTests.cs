﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;

namespace Management.BLL.Tests
{
    public class ProjectServiceTests
    {
        readonly ProjectService _projectService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public ProjectServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _projectService = new ProjectService(_uow, _mapper);
        }
        [Fact]
        public void GetAllProjectsAndItsTasksInfo_ThenAllProjects()
        {
            Assert.Equal(100, _projectService.GetAllProjectsAndItsTasksInfo().Count);
        }
        [Fact]
        public void GetAllProjectsAndItsTasksInfo_WhenProjectNameMoreThan20_ThenUsersInTeamNotZero()
        {
            Assert.NotEqual(0, _projectService.GetAllProjectsAndItsTasksInfo().ToList()[20].UserAmount);
        }
    }
}
