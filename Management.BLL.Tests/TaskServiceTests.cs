﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
using System.Collections.Generic;
using FakeItEasy;
using Management.DAL.Models;

namespace Management.BLL.Tests
{
    public class TaskServiceTests
    {
        readonly TaskService _taskService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public TaskServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _taskService = new TaskService(_uow, _mapper);
        }
        [Theory]
        [InlineData(1)]
        public void GetAllTasksWithNameLessThan45Chars(int Id)
        {
            bool check = _taskService.GetAllTasksWithNameLessThan45Chars(Id).ToList()[0].Name.Length < 45;
            Assert.True(check);
        }
        [Fact]
        public void UpdateTaskState_WhenFinished_ThenNewTaskStateIsFinished()
        {
            TaskDTO task = _taskService.GetTasks().First();
            task.State = TaskStateDTO.Finished;
            _taskService.UpdateTask(task);
            Assert.Equal(TaskStateDTO.Finished, _taskService.GetTaskById(task.Id).State);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public void GetNotFinishedTasksForUser_WhenTaskIsPresent_ThenUserIsPerformer(int id)
        {
            List<TaskDTO> tasks = _taskService.GetNotFinishedTasksForUser(id).ToList();
            Assert.Equal(id, tasks[0].PerformerId);
        }
       
        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public void GetNotFinishedTasksForUser_WhenWeGotTaskList_ThenFilterOnAllIsCalled(int id)
        {
            ICollection<TaskDTO> tasks = _taskService.GetNotFinishedTasksForUser(id);
            Assert.NotNull(tasks);
            
        }
        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public void GetNotFinishedTasksForUser_WhenWeGotTaskList_ThenAllIsNotFinished(int id)
        {
            List<TaskDTO> tasks = _taskService.GetNotFinishedTasksForUser(id).ToList();
            Assert.NotEqual(TaskStateDTO.Finished,tasks[0].State);

        }
    }
}
