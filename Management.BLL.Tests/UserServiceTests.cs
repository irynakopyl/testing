using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;

namespace Management.BLL.Tests
{
    public class UserServiceTests
    {
        readonly UserService _userService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public UserServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _userService = new UserService(_uow, _mapper);
        }

        [Fact]
        public void AddUser_WhenNewUser_ThenUsersPlusOne()
        {
            int amount = _userService.GetUsers().Count;
            var user = new UserDTO
            {
                FirstName = "Lol",
                LastName = "Kekovich",
                Email = "lol@kek.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1,
            };
            _userService.CreateUser(user);
            Assert.Equal(amount + 1, _userService.GetUsers().Count);
        }
        [Fact]
        public void AddUser_WhenDeleteExistingUser_ThenUsersCountNotChanges()
        {
            int amount = _userService.GetUsers().Count;
            var user = new UserDTO
            {
                FirstName = "Lol",
                LastName = "Kekovich",
                Email = "lol@kek.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1,
            };
            user = _userService.CreateUser(user);
            _userService.DeleteUser(user.Id);
            Assert.Equal(amount, _userService.GetUsers().Count);
        }
        [Fact]
        public void UpdateUserToTeam_WhenNewTeam_ThenNewTeamId()
        {
            UserDTO usr = _userService.GetUsers().First();
            usr.TeamId = 10;
            UserDTO lol = new UserDTO
            {
                Birthday = usr.Birthday,
                Email = usr.Email,
                FirstName = usr.FirstName,
                LastName = usr.LastName,
                RegisteredAt = usr.RegisteredAt,
                TeamId = usr.TeamId
            };
            _userService.UpdateUser(usr);
            Assert.Equal(10, _userService.GetUserById(usr.Id).TeamId);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(55)]
        public void GetTaskAmountInProjects_EqualsProjectAmount(int id)
        {
            Assert.Equal(100, _userService.GetTaskAmountInProjects(id).Count);
        }

        [Theory]
        [InlineData(15)]
        public void GetAllTasksFinishedInCurrentYear_WhenUser15_EqualsTwo(int id)
        {
            Assert.Equal(2, _userService.GetAllTasksFinishedInCurrentYear(id).Count);
        }
        [Fact]
        public void GetAllSortedUsersWithSortedTasks_AllUsersReturned()
        {
            Assert.Equal(_userService.GetUsers().Count, _userService.GetAllSortedUsersWithSortedTasks().Count);
        }
        [Theory]
        [InlineData(23)]
        [InlineData(37)]
        public void GetAllSortedUsersWithSortedTasks_WhenSorted_TaskLengthAreGreater(int id)
        {
            var task1 = _userService.GetAllSortedUsersWithSortedTasks().ToList()[id].Tasks[1].Name.Length;
            var task2 = _userService.GetAllSortedUsersWithSortedTasks().ToList()[id].Tasks[2].Name.Length;
            Assert.True(task1 >= task2);
        }
        [Theory]
        [InlineData(19)]
        public void GetTasksAndProjectsInfo(int id)
        {
            Assert.Equal(id, _userService.GetTasksAndProjectsInfo(id).User.Id);
        }

    }
}
