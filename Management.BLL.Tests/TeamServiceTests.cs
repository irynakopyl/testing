﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
namespace Management.BLL.Tests
{
    public class TeamServiceTests
    {
        readonly TeamService _teamService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public TeamServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _teamService = new TeamService(_uow, _mapper);
        }
        [Fact]
        public void CreateTeam_WhenCreated_ThenCountPlusOne()
        {
            int amount = _teamService.GetTeams().Count;
            var team = new TeamDTO
            {
                Name = "NewTeam",
                CreatedAt = DateTime.Now,
            };
            var newTeam = _teamService.CreateTeam(team);
            Assert.Equal(amount + 1, _teamService.GetTeams().Count);
        }
        [Fact]
        public void GetTeamsWithUsersOlderThan10()
        {
            bool check = (DateTime.Now.Year - _teamService.GetTeamsWithUsersOlderThan10().FirstOrDefault().Members.FirstOrDefault().Birthday.Year)>10;
            Assert.True(check);
        }
    }
}
