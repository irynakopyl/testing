﻿using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.InMemory;

namespace Management.BLL.Tests
{
    public class ConnectionFactory : IDisposable
    {
        private bool disposedValue = false; // To detect redundant calls  

        public ManagementDbContext CreateContextForInMemory()
        {
            var option = new DbContextOptionsBuilder<ManagementDbContext>().UseInMemoryDatabase(databaseName: "Test_Database").Options;

            var context = new ManagementDbContext(option);
            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
