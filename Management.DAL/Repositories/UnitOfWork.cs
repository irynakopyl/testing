﻿using Management.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Models;
using System.Xml.Linq;

namespace Management.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;
        public IRepository<User> UsersRepo { get; set; }
        public IRepository<Team> TeamsRepo { get; set; }
        public IRepository<Project> ProjectsRepo { get; set; }
        public IRepository<Task> TasksRepo { get; set; }
        private ManagementDbContext _context;
        public UnitOfWork(ManagementDbContext context)
        {   
            UsersRepo = new UserRepository(context);
            TeamsRepo = new TeamRepository(context);
            ProjectsRepo = new ProjectRepository(context);
            TasksRepo = new TaskRepository(context);
            _context = context;
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
